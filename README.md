## CAD/CAM/CNC
* [Free Mechanical Engineering CAD/CAM Software](http://www.freebyte.com/cad/cadcam.htm) - list of very useful, free CAD/CAM/CNC software

## Data science
* [Towards Data Science](https://towardsdatascience.com/)

#### Articles
* [Logistic Regression using Python](https://towardsdatascience.com/logistic-regression-using-python-sklearn-numpy-mnist-handwriting-recognition-matplotlib-a6b31e2b166a)

## Math, Economics, Financial
* [Probabilistic Foundations of Econometrics: Part 1](https://dzone.com/articles/probabilistic-foundations-of-econometrics-part-1)
* [Findatapy - Python API to download market data from many sources including Quandl, Bloomberg, Yahoo, Google etc.](https://github.com/cuemacro/findatapy)

## Other
* [The secret world of microwave networks](https://arstechnica.com/information-technology/2016/11/private-microwave-networks-financial-hft/)
* [90+ TOOLS TO HELP YOU SUCCEED](https://foundersbelt.com/timeline?stage=learning)